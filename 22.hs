import System.IO

import qualified Data.Map as Map
import qualified Data.Maybe as Maybe

type Cube = ((Integer,Integer),(Integer,Integer),(Integer,Integer))

ms_union = Map.unionWith (+)
ms_insert x xx = Map.insertWith (+) x 1 xx
ms_insertMany x v xx = Map.insertWith (+) x v xx

ms_mapMaybe :: (Cube -> Maybe Cube) -> Map.Map Cube Integer -> Map.Map Cube Integer
ms_mapMaybe fn xx = Map.foldrWithKey (\k v acc -> maybe acc (\x->ms_insertMany x v acc) (fn k)) Map.empty xx

intersect :: Cube -> Cube -> Maybe Cube
intersect (x1,y1,z1) (x2,y2,z2)
    | Maybe.isNothing xx || Maybe.isNothing yy || Maybe.isNothing zz = Nothing
    | otherwise = let Just xxx=xx; Just yyy=yy; Just zzz=zz in Just (xxx,yyy,zzz)
    where
    intersect' (a1,a2) (b1,b2)
        | max1 <= min2 = Just (max1, min2)
        | otherwise = Nothing
        where
        max1 = max a1 b1
        min2 = min a2 b2
    xx = intersect' x1 x2
    yy = intersect' y1 y2
    zz = intersect' z1 z2

parsel l = (let [s,_,x1,x2,_,y1,y2,_,z1,z2]=words (map repl l) in
    (s,((r x1, r x2),(r y1, r y2),(r z1, r z2))))
    where
    r x = read x::Integer
    repl '.' = ' '
    repl '=' = ' '
    repl ',' = ' '
    repl c = c

clean (adds0, subs0) = Map.foldrWithKey (\k v (adds,subs) -> maybe
    (Map.insert k v adds, subs)
    (\v2 -> if v2==v then (adds, Map.delete k subs) else
        if v2>v then (adds, Map.insert k (v2-v) subs)
        else (Map.insert k (v-v2) adds, Map.delete k subs))
    (Map.lookup k subs)) (Map.empty, subs0) adds0

apply (adds, subs) (s,ss)
    -- on0 - first add cancel out all the minus-ed parts (asub),
    -- on1 - add the full thing, then minus away those that alr exist (aadd)
    -- off0 - add back all the wrongly minus-ed parts (asub)
    -- off1 - minus all the wrongly add-ed parts (asub)
    | s == "on" = clean (ms_insert ss (ms_union asub adds), ms_union aadd subs)
    | otherwise = clean (ms_union asub adds, ms_union aadd subs)
    where
    aadd = ms_mapMaybe (\x -> intersect ss x) adds
    asub = ms_mapMaybe (\x -> intersect ss x) subs

getsize (adds, subs) = (sums adds) - (sums subs)
    where
    sum' (a,b) = b-a+1
    sums = Map.foldrWithKey (\(x,y,z) v acc->acc+(v*(sum' x)*(sum' y)*(sum' z))) 0

main1 ll = getsize state
    where
    is_init = all (\x -> x>=(-50) && x<=50)
    ll0 = filter (\(_,((x1,x2),(y1,y2),(z1,z2))) -> is_init [x1,x2,y1,y2,z1,z2]) ll
    state = foldl apply (Map.empty, Map.empty) ll0

main2 ll = getsize state
    where
    state = foldl apply (Map.empty, Map.empty) ll

main = do
    din <- readFile "22.txt"
    let ll = map parsel$ lines din
    print(main2 ll)
