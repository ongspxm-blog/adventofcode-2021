import System.IO
import Data.List

getnums line = read (intercalate ""$ map (\w -> show$ getnum (line!!w) line) [11..14])::Int

getnum outcode line
    | ll == 2 = 1
    | ll == 4 = 4
    | ll == 3 = 7
    | ll == 7 = 8
    | ll == 5 = if is_b then 5 else if is_e then 2 else 3
    | ll == 6 = if not is_e then 9 else if is_ca then 0 else 6
    where
    ll = length outcode
    nums = process line

    c_b = snd$ (filter (\x -> fst x==6) nums)!!0
    c_e = snd$ (filter (\x -> fst x==4) nums)!!0
    c_f = snd$ (filter (\x -> fst x==9) nums)!!0

    c_a = snd$ (filter (\x -> fst x==8) nums)!!0
    c_c = snd$ (filter (\x -> fst x==8) nums)!!1

    is_b = (length$ filter (==c_b) outcode)==1
    is_e = (length$ filter (==c_e) outcode)==1
    is_f = (length$ filter (==c_f) outcode)==1
    is_ca = (length$ filter (\x -> x==c_c || x==c_a) outcode)==2

process x = map (\c -> (length c,c!!0))$ group$ sort$ concat$ map (x!!) [0..9]

main1 = do
    din <- readFile "08.txt"
    let dlines = map words (lines din)
    let doutput = concat$ map (\line -> map (line !!) [11..14]) dlines
    print(length$ filter (\x -> x==2 || x==4 || x==3 || x==7)$ map length doutput)

main2 = do
    din <- readFile "08.txt"
    let dlines = map words (lines din)
    print(sum$ map getnums dlines)
