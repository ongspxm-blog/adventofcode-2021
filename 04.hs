import System.IO
import Debug.Trace

repl ',' = ' '
repl c = c

process line = if length line > 0 then map (\x -> read [line!!(x*3),line!!((x*3)+1)]::Int) [0..4] else []

isdone_c vals = not$ all (\x -> any (/= -1) [vals!!(y*5+x) | y<-[0..4]]) [0..4]
isdone_r vals = not$ all (\y -> any (/= -1) [vals!!(y*5+x) | x<-[0..4]]) [0..4]
isdone vals = isdone_c vals || isdone_r vals

score vals = sum$ filter (/= -1) vals

soln1 :: [Int] -> [[Int]] -> Int
soln1 (v:vs) blks = if (length wins > 0) then score (wins!!0)*v else (soln1 vs blks2)
    where
        cc c = if c==v then -1 else c
        blks2 = map (map cc) blks
        wins = filter isdone blks2

soln2 :: [Int] -> [[Int]] -> Int
soln2 (v:vs) blks = if (length loses==0) then score (blks2!!0)*v else (soln2 vs loses)
    where
        cc c = if c==v then -1 else c
        blks2 = map (map cc) blks
        loses = filter (not.isdone) blks2

main = do
    din <- readFile "04.txt"
    let d_lines = lines din
    let vals = map (\x -> read x::Int) $ words $ map repl (d_lines!!0)

    let _:d_blks0 = d_lines
    let d_blks = map process d_blks0
    let blks = map (\s -> concat$ map (\x -> d_blks!!((s*6)+x)) [1..5]) [0..(div (length d_blks) 6)-1]
    print(soln2 vals blks)
