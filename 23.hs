import System.IO
import Debug.Trace

import qualified Data.Map as Map
import qualified Data.List as List
import qualified Data.Heap as Heap

type State = ([Int], [Int], [Int], [Int], [Int])
rfirst curr (x:xx) = if x==0 then rfirst (0:curr) xx else (0:curr)++xx

astar (h,w,x,y,z) = (sum hscore) + (sum$ map score'$ zip [1,10,100,1000] [w,x,y,z])
    where
    hpos = Map.fromList [(1,2),(10,4),(100,6),(1000,8)]
    score' (i,xx)
        | all (\v -> v==0 || v==i) xx = 0
        | otherwise = sum$ map (\(e,v) -> if v==0 then 0
            else v*(abs ((hpos Map.! v)-rpos)+e)) (zip [2..] xx)
        where
        rpos = hpos Map.! i

    hscore = map (\(i,v) -> if v==0 then 0 else (let i2=hpos Map.! v in v*((abs (i2-i))+1))) (zip [0..] h)

next :: State -> [(Int,State)]
next (h,w,x,y,z) = hr_moves ++ rh_moves
    where
    replace i vv xx = map (\(ii,v)-> if ii==i then vv else v) (zip [0..] xx)

    hpoten = [0,1,3,5,7,9,10]
    hpos = Map.fromList [(1,2),(10,4),(100,6),(1000,8)]

    rooms = zip [1,10,100,1000] [w,x,y,z]
    rcheck v xx@(a:_)
        | not (all (\x -> x==0 || x==v) xx) = -1
        | a==v = 0
        | otherwise = length$ takeWhile (==0) xx
    rvalid = Map.fromList (map (\(i,r)->(i,rcheck i r)) rooms)

    -- return travel from hall pos i to right outside room of room v
    gethall :: (Int, Int) -> Int
    gethall (i,b) = if d==0 then s else -1
        where
        s = abs (i-b)
        d = sum$ take (s-1) (reverse$ take (max b i) h)

    hr_move i = (h0, w0, x0, y0, z0)
        where
        h0 = replace i 0 h
        fill_room (a:aa) v
            | a/=0 = undefined
            | otherwise = aa++[v]

        iv = h !! i
        w0 = if iv /= 1 then w else fill_room w iv
        x0 = if iv /= 10 then x else fill_room x iv
        y0 = if iv /= 100 then y else fill_room y iv
        z0 = if iv /= 1000 then z else fill_room z iv
    hr_moves = map (\(i,v,d0,d1) -> (v*(d0+d1), hr_move i)) (
        filter (\(i,v,d0,d1) -> d0/=(-1) && d1/=(-1)) (
        map (\(i,v) -> if v==0 then (i,v,-1,-1) else
            (i, v, gethall (i, hpos Map.! v), rvalid Map.! v))
        (zip [0..] h)))

    rh_move :: (Int, [Int]) -> [(Int,State)]
    rh_move (iv,xx) = map (\tp -> (
            vv*(d0+(gethall (cp,tp))),
            (replace tp vv h,w0,x0,y0,z0)))
        (filter (\x -> (h!!x)==0 && (gethall (cp,x)) /= (-1)) hpoten)
        where
        cp = hpos Map.! iv

        r2 = rfirst [] xx
        d0 = 1 + (length$ takeWhile (==0) xx)
        vv = foldl (\acc x -> if acc==0 then x else acc) 0 xx

        w0 = if iv /= 1 then w else r2
        x0 = if iv /= 10 then x else r2
        y0 = if iv /= 100 then y else r2
        z0 = if iv /= 1000 then z else r2
    rh_moves = concat$ map rh_move (filter (\(v,_)->(rvalid Map.! v)==(-1)) rooms)

parse :: [[Int]] -> String -> State
parse extra ll = let [w,x,y,z]=List.transpose ((repl' a):extra++[(repl' b)]) in ([0,0,0,0,0,0,0,0,0,0,0],w,x,y,z)
    where
    [_,_,a,b] = take 4$ lines ll
    repl '#' = ' '
    repl c = c

    score = Map.fromList [("A",1), ("B",10), ("C",100), ("D",1000)]
    repl' x = map (\x->score Map.! x)$ words (map repl x)

process :: Heap.MinHeap (Int, Int, State) -> Map.Map State Int -> State -> Int
process pq scores goal
    | c_state == goal = c_score
    | trace (show curr) False = undefined
    | (scores Map.! c_state) /= c_score = process pq0 scores goal
    | Heap.isEmpty pq = undefined
    | otherwise = process pq1 (
       foldl (\acc (_,nsc,nst)->Map.insert nst nsc acc) scores states) goal
    where
    (curr, pq0) = maybe undefined (\x->x) (Heap.view pq)
    (c_astar, c_score, c_state) = curr

    states = filter (\(_,nsc,nst)-> if Map.notMember nst scores then True
        else (scores Map.! nst)>nsc )$ map (\(n_score, n_state)->(
        (n_score+c_score+astar n_state), (n_score+c_score), n_state)) (next c_state)

    pq1 = Heap.union pq0 (Heap.fromList states)

getgoal i = (fn 11 0, fn i 1, fn i 10, fn i 100, fn i 1000)
    where fn a b = take a (repeat b)

mainfn extra cnt din = process (
        Heap.singleton (0,0,state0) :: Heap.MinHeap (Int,Int,State))
        (Map.singleton state0 0) (getgoal cnt)
    where state0 = parse extra din

main1 = mainfn [] 2
main2 = mainfn [[1000,100,10,1],[1000,10,1,100]] 4

main = do
    din <- readFile "23.txt"
    print(main2 din)
