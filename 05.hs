import System.IO
import Data.List

pline line = res
    where
    repl ',' = ' '
    repl c = c

    ws = words line
    ccoord ww = map (\x -> read x::Int) (words$ map repl ww)

    [x0,y0] = ccoord (ws!!0)
    [x1,y1] = ccoord (ws!!2)

    dx = x1-x0
    dy = y1-y0

    ex = if dx>0 then 1 else -1
    ey = if dy>0 then 1 else -1

    res = if (dx/=0 && dy/=0) then [(
            ex*ee+x0, ey*ee+y0) | ee<-[0..abs(dx)]]
        else (if abs(dx)>0 then
            [(ex*xx+x0,y0)| xx<-[0..abs(dx)]]
            else [(x0,ey*yy+y0)| yy<-[0..abs(dy)]])

convert coords = map (\(x,y) -> x+(y*xx)) coords
    where xx = maximum (map (\x -> fst x) coords)

main = do
    din <- readFile "05.txt"
    let dlines = lines din
    let dcoords = convert (concat$ map pline dlines)
    let dcounts = map length (group$ sort dcoords)
    print(length$ filter (>1) dcounts)
