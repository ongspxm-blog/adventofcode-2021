import System.IO
import Debug.Trace

import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.List as List

type Pos = (Integer, Integer, Integer)
dist :: Pos -> Pos -> Integer
dist (a,b,c) (d,e,f) = ((a-d)^2) + ((b-e)^2) + ((c-f)^2)

diff :: Pos -> Pos -> Pos
diff (a,b,c) (d,e,f) = ((a-d),(b-e),(c-f))

-- matrix stuff
m_det :: [[Integer]] -> Integer
m_det [[a,b],[c,d]] = (a*d) - (b*c)
m_det [[a,b,c],[d,e,f],[g,h,i]] = (
    (a*m_det [[e,f],[h,i]])
    - (b*m_det [[d,f],[g,i]])
    + (c*m_det [[d,e],[g,h]]))

m_mul :: [[Integer]] -> [[Integer]] -> Integer -> [[Integer]]
m_mul [[a,b,c],[d,e,f],[g,h,i]] [[a2,b2,c2],[d2,e2,f2],[g2,h2,i2]] dd = [
    (map (\x -> div x dd) [a*a2+b*d2+c*g2, a*b2+b*e2+c*h2, a*c2+b*f2+c*i2]),
    (map (\x -> div x dd) [d*a2+e*d2+f*g2, d*b2+e*e2+f*h2, d*c2+e*f2+f*i2]),
    (map (\x -> div x dd) [g*a2+h*d2+i*g2, g*b2+h*e2+i*h2, g*c2+h*f2+i*i2])]
m_mul [[a,b,c]] [[a2,b2,c2],[d2,e2,f2],[g2,h2,i2]] dd = [
    (map (\x -> div x dd) [a*a2+b*d2+c*g2, a*b2+b*e2+c*h2, a*c2+b*f2+c*i2])]

m_inv :: [[Integer]] -> [[Integer]]
m_inv arr@[[a,b,c],[d,e,f],[g,h,i]]
    | det == 0 = undefined
    | otherwise = List.transpose$ map (\y -> map (\x -> (m_det$ cover x y)*(if mod (x+y) 2 == 0 then 1 else -1)) [0..2]) [0..2]
    where
    det = m_det arr
    arr0 = zip [0..] (map (\l -> zip [0..] l) arr)
    cover x y = (map
        (\vv -> map (\(_,v)->v)$ (filter (\(i,_)->i/=x) vv))$
        (map (\(_,v)->v)$ filter (\(i,_)->i/=y) arr0))

fingerprint :: [Pos] -> [Integer]
fingerprint f = fingerprint' f
    where
    fingerprint' [] = []
    fingerprint' (b:bb) = (map (\x -> dist b x) bb) ++ (fingerprint bb)

fsame :: [Integer] -> [Integer] -> [Integer] -> [Integer]
fsame f1 [] curr = curr
fsame f1 f2@(f:ff) curr = if elem f f1 then fsame (List.delete f f1) ff (f:curr) else fsame f1 ff curr

sensor_pos :: [Pos] -> [Pos] -> (Bool, [[Integer]], Pos)
sensor_pos ss0 ss1 = if length c1 == 0 then (False, [[0]], (0,0,0)) else (True, rot, new0)
    where
    (c1, c2) = overlap ss0 ss1
    d_fn (s:ss) = let (x,_)=foldl (\(acc, last) x -> ((diff last x):acc, x)) ([],s) ss in x
    split3 (p1:p2:p3:pp) = foldl (\curr@((_:aa):_) r -> (aa++[c r]):curr) [c0] pp
        where
        c (a,b,c) = [a,b,c]
        c0 = map c [p1,p2,p3]

    -- returns A where s1(A) = s2
    sensor_pos' s1 s2 = (map (\((a,d),b)->m_mul a b d)$ filter (\((_,d),_) -> d/=0)$ zip (map (\x -> (m_inv x, m_det x)) (split3 (d_fn s1))) (split3 (d_fn s2)))!!0
    rot = sensor_pos' c2 c1

    cvn (a,b,c) = [[a,b,c]]
    new0 = let [a:b:c:_]=(m_mul (cvn (c2!!0)) rot 1) in diff (c1!!0) (a,b,c)


overlap :: [Pos] -> [Pos] -> ([Pos],[Pos])
overlap s1 s2 = (c1,c2)
    where
    dists = fsame (fingerprint s1) (fingerprint s2) []
    c1 = fsort$ overlap0 s1
    c2 = fsort$ overlap0 s2

    overlap0 ss = concat$ map (\x -> if length x > 10 then [x!!0] else [])$ List.group$ List.sort$ overlap' ss

    overlap' [] = []
    overlap' (f:ff) = (concat (map (\x -> let d=dist f x in (if elem d dists then [f,x] else [])) ff)) ++ (overlap' ff)

    fsort ss = map (\(_,s)->s)$ List.sort$ map (\s -> (List.sort$ map (\s2 -> dist s s2) ss, s)) ss

parse :: [Char] -> Pos
parse x = (a,b,c)
    where
    [a,b,c]=(map (\w -> read w::Integer) (words (map repl x)))
    repl ',' = ' '
    repl c = c

process curr_s data_s visited map_s set_b
    | (length visited) == (length data_s) = (visited, map_s, set_b)
    | otherwise = foldl (\(vs, ms, sb) x -> process x data_s vs ms sb) (visited1, map_s1, set_b1) next 
    where
    curr_b = data_s Map.! curr_s
    ((s0,s1,s2), c_matrix) = map_s Map.! curr_s

    poten_s = Map.filterWithKey (\k _ -> Set.notMember k visited) data_s

    fn curr@(map_s0, set_b0, visited0, newvisit) s_id s_bs
        | not b_isokay = curr
        | otherwise = (
            (Map.insert s_id ((ba+s0,bb+s1,bc+s2),b_matrix) map_s0),
            (Set.union set_b0 (Set.fromList (map convert s_bs))),
            (Set.insert s_id visited0), s_id:newvisit)
        where
        (b_isokay, b_matrix0, (ba0,bb0,bc0)) = sensor_pos curr_b s_bs
        b_matrix = m_mul b_matrix0 c_matrix 1
        [[ba,bb,bc]] = m_mul [[ba0,bb0,bc0]] c_matrix 1
        convert (a,b,c) = let [[d,e,f]]=(m_mul [[a,b,c]] b_matrix 1) in (
            ba+d+s0,bb+e+s1,bc+f+s2)

    (map_s1, set_b1, visited1, next) = Map.foldlWithKey fn (map_s, set_b, visited, []) poten_s


main1 (_,_,set_b) = length set_b
main2 (_,map_s,_) = fn$ map (\(_,(p,_)) -> p) (Map.toList map_s)
    where 
    fn (p:[]) = 0
    fn (p:pp) = max (maximum$ map (\x -> let (a,b,c)=(diff p x) in (abs(a)+abs(b)+abs(c))) pp) (fn pp)

main = do
    din <- readFile "19.txt"
    let sensors = Map.fromList$ zip [0..] (reverse$ (map (\(_:l)->map parse l)$ foldl (\curr@(c:cc) x -> if x=="" then []:curr else (c++[x]):cc) [[]] (lines din)))
    
    let vals=process 0 sensors (Set.singleton 0) (Map.singleton 0 ((0,0,0),[[1,0,0],[0,1,0],[0,0,1]])) (Set.fromList$ sensors Map.! 0)
    print(main2 vals)

