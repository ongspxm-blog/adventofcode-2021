import System.IO
import Debug.Trace

import qualified Data.List as List

process' m vals@(f:_) = let (a,_,moved)=foldr fn ([f],[f],False) vals in 
    (if f=='.' then (last a):(init (tail a)) else init a, moved)
    where
    fn x (acc@(c:cc),dd@(d:_),moved)
        | d=='.' && x == m = (('.':x:cc), ddd, True)
        | otherwise = ((x:acc), ddd, moved)
        where ddd=x:dd

next grid = (List.transpose grid2, moved1 || moved2)
    where
    fn gg c = foldr (\l (acc0,acc1) -> let (a,b)=process' c l in (a:acc0, acc1 || b)) ([],False) gg
    (grid1, moved1) = fn grid '>'
    (grid2, moved2) = fn (List.transpose$ grid1) 'v'

main1 grid cnt = if moved then main1 grid1 (cnt+1) else cnt
    where (grid1, moved) = next grid

main = do
    din <- readFile "25.txt"
    let grid = lines din
    print(main1 grid 1)
