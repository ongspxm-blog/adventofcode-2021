import System.IO
import Data.List.Ordered
import Debug.Trace

repl ',' = ' '
repl c = c

getval1 vals v = sum$ map (\x -> abs(v-x)) vals
getval2 vals v = sum$ map (\x -> sum [0..abs(v-x)]) vals

grad :: [Int] -> Int -> ([Int] -> Int -> Int) -> Int
grad vals v f = w1+w2
    where
    v0 = f vals (v-1)
    v1 = f vals v
    v2 = f vals (v+1)
    w1 = if v1>v0 then 1 else -1
    w2 = if v2>v1 then 1 else -1

binsearch :: [Int] -> Int -> Int -> ([Int] -> Int -> Int) -> Int
binsearch vals v0 v1 f
    | trace ("- " ++ show v0 ++ " " ++ show v1 ++ " " ++ show m) False = undefined
    | g==0 = f vals m
    | g>0 = binsearch vals v0 m f
    | otherwise = binsearch vals m v1 f
    where
    m = div (v0+v1) 2
    g = grad vals m f

main = do
    din <- readFile "07.txt"
    let vals = map (\x -> read x::Int)$ words$ map repl din
    let nvals = nubSort vals
    let dists = map (\v -> getval2 vals v) [minimum nvals..maximum nvals]
    let ans = binsearch vals (minimum nvals) (maximum nvals) getval2
    print(ans)
