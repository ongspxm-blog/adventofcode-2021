import System.IO

readInt :: String -> Int
readInt = read

main1 = do
    txt <- readFile "01.txt"
    let trig = map readInt (lines txt)
    let ans = foldl (\acc x -> ((fst acc)+(if snd acc < x then 1 else 0), x)) (0,trig !! 0) trig
    print(fst ans)

main2 = do
    txt <- readFile "01.txt"
    let trig0 = map readInt (lines txt)
    let trig = map (\x -> (trig0 !! (x-3)) - (trig0 !! x)) [3..(length trig0 - 1)]
    let ans = length $ filter (<0) trig 
    print(ans)
