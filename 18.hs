import System.IO
import Debug.Trace

data Fish a = FishVal Int | FishCom (Fish a) (Fish a) deriving (Show,Eq)

parse :: [Char] -> (Fish a, [Char])
parse str@(s:ss)
    | s=='[' = let (fl,ss2)=parse ss; (fr,str2)=parse ss2; in (FishCom fl fr, str2)
    | s==',' || s==']' = parse ss
    | otherwise = (FishVal (read [s]::Int), ss)


explode :: Int -> Fish a -> (Fish a, Bool, Int, Int)
explode depth f@(FishVal _) = (f, False, 0, 0)
explode depth f@(FishCom (FishVal f1) (FishVal f2))
    | depth >= 4 = (FishVal 0, True, f1, f2)
    | otherwise = (f, False, 0, 0)
explode depth f@(FishCom f1 f2)
    | f1c == True = (FishCom f11 (addLeft f2 f1b),True,f1a,0)
    | f2c == True = (FishCom (addRight f1 f2a) f21,True,0,f2b)
    | otherwise = (f,False,0,0)
    where
    (f11,f1c,f1a,f1b)=explode (depth+1) f1
    (f21,f2c,f2a,f2b)=explode (depth+1) f2

    addLeft (FishVal f) v = FishVal (f+v)
    addLeft (FishCom f1 f2) v  = FishCom (addLeft f1 v) f2

    addRight (FishVal f) v = FishVal (f+v)
    addRight (FishCom f1 f2) v  = FishCom f1 (addRight f2 v)

split :: Fish a -> (Fish a, Bool)
split f@(FishVal v)
    | v>9 = let x=(div v 2) in
        (FishCom (FishVal x) (FishVal (x+(if even v then 0 else 1))), True)
    | otherwise = (f, False)
split f@(FishCom f1 f2)
    | f1c == True = (FishCom f11 f2, True)
    | f2c == True = (FishCom f1 f21, True)
    | otherwise = (f, False)
    where
    (f11, f1c) = split f1
    (f21, f2c) = split f2

reduce :: Fish a -> Fish a
reduce f
    | fec == True = reduce fe0
    | fsc == True = reduce fs0
    | otherwise = f
    where
    (fe0, fec, _, _) = explode 0 f
    (fs0, fsc) = split f

getmag :: Fish a -> Integer
getmag (FishVal v) = fromIntegral v::Integer
getmag (FishCom f1 f2) = (3*(getmag f1)) + (2*(getmag f2))

maxmag :: [Fish a] -> Integer
maxmag (f:[]) = 0
maxmag (f:ff) = max (maximum$ map (\x -> max
    (getmag$ reduce$ FishCom f x)
    (getmag$ reduce$ FishCom x f)) ff) (maxmag ff)

test0 = "[[[[[9,8],1],2],3],4]"
test1 = "[7,[6,[5,[4,[3,2]]]]]"
test2 = "[[6,[5,[4,[3,2]]]],1]"
test3 = "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]"
test4 = "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"
test5 = "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]"

main1 (f:ff) = getmag$ foldl (\curr x -> reduce (FishCom curr x)) f ff
main2 fishes = maxmag fishes

main = do
    din <- readFile "18.txt"
    let fishes = map (\x->let (f,_)=parse x in f)$ lines din
    print(main2 fishes)
