import System.IO
import Data.List

toclose c = case c of {
    '(' -> ')';
    '[' -> ']';
    '{' -> '}';
    '<' -> '>';
    _ -> ' ';
}

score1 c = case c of {
    ')' -> 3;
    ']' -> 57;
    '}' -> 1197;
    '>' -> 25137;
    _ -> 0
}

score2' c = case c of {
    ')' -> 1;
    ']' -> 2;
    '}' -> 3;
    '>' -> 4;
}

score2 v = foldl (\acc x -> (score2' x)+acc*5) 0 v

findfirst :: [Char] -> [Char] -> [Char]
findfirst v [] = v
findfirst [] (x:xs) = findfirst [toclose x] xs
findfirst stack@(s:ss) (x:xs)
    | c /= ' ' = findfirst (c:stack) xs
    | otherwise = if x==s then (findfirst ss xs) else ""
    where c = toclose x

main = do
    din <- readFile "10.txt"
    let dlines = lines din
    let errs = map (findfirst []) dlines
    let scores = sort$ filter (>0)$ map score2 errs
    print(scores!!(div (length scores) 2))

