import System.IO
import Debug.Trace

import qualified Data.Map as Map
import qualified Data.List as List

pos0 = (4,8,0,0,0)
pos1 = (5,10,0,0,0)

qdice = map (\xx -> (xx!!0, length xx))$ List.group$ List.sort [x1+x2+x3|x1<-[1..3],x2<-[1..3],x3<-[1..3]]

insertMany=Map.insertWith (+)

process (games,wins)
    | Map.size games==0 = fn1 wins
    | otherwise = process (Map.foldrWithKey fn (Map.empty,wins) games)
    where
    fn state@(pos1,pos2,score1,score2,turn) occ (games1,wins1)
        | score2 >= 21 = (games1,insertMany state occ wins1)
        | otherwise = ((foldl (\acc (x,occ1)->let p=rr (mod (pos1+x) 10) in (
            insertMany (pos2,p,score2,score1+p,1-turn) (occ*occ1) acc)) games1 qdice),wins1)
        where rr x = if x==0 then 10 else x
    fn1 w = Map.foldrWithKey (\(_,_,_,_,i) occ acc->Map.insertWith (+) i occ acc) Map.empty w

main1 state1 = if s2>=1000 then s1*r else main1 state2
    where
    next (state0@(pos1,pos2,score1,score2,rolls),(d1:d2:d3:dice)) = (
            (pos2,p1, score2,(score1+p1),(rolls+3)), dice)
        where
        p = mod (pos1+d1+d2+d3) 10
        p1 = if p==0 then 10 else p
    state2@((_,_,s1,s2,r),_)=next state1

main = do
    -- print(main1 (pos1,cycle [1..100]))
    print(process (Map.singleton pos1 1,Map.empty))
