import System.IO

solver :: Int -> Int -> Int -> Double
solver a b c = let a' = fromIntegral a; b' = fromIntegral b; c' = fromIntegral c in (
    (sqrt ((b'^2) - (4*a'*c')) - b')/(2*a'))

ext ss = map (\x -> read x::Int)$ words$ map repl ss
    where
    repl '.' = ' '
    repl ',' = ' '
    repl c = c

main1 x_1 x_2 y_1 y_2 = let y1 = (-1*y_1)-1 in div (y1*(y1+1)) 2

main2 x_1 x_2 y_1 y_2 = length$ filter (==True) [
    isvalid (dx,dy) (0,0) | dx<-[x0..x1], dy<-[y0..y1]]
    where
    (x0,x1) = (ceiling$ solver 1 1 (-2*x_1), x_2)
    (y0,y1) = (y_1, (-1*y_1)-1)

    isvalid (dx,dy) (x,y)
        | x>x_2 || y<y_1 = False
        | x>=x_1 && y<=y_2 = True
        | otherwise = isvalid (if dx>1 then dx-1 else 0,dy-1) (x+dx,y+dy)

main = do
    din <- readFile "17.txt"
    let [_,_,(_:_:xx),(_:_:yy)] = words din
    let [x_1,x_2] = ext xx
    let [y_1,y_2] = ext yy
    print(main2 x_1 x_2 y_1 y_2)
