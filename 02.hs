import System.IO

sim ("forward",s) (x,y) = (x+s,y)
sim ("down",s) (x,y) = (x,y+s)
sim ("up",s) (x,y) = (x,y-s)

main1 = do
    din <- readFile "02.txt"
    let cmds = map (\x -> (x !! 0, read (x!!1) :: Int)) $ map words (lines din)
    let final = foldl (\pos cmd -> sim cmd pos) (0,0) cmds
    print(fst final * snd final)

sim2 ("forward",s) (x,y,t) = (x+s,y+(t*s),t)
sim2 ("down",s) (x,y,t) = (x,y,t+s)
sim2 ("up",s) (x,y,t) = (x,y,t-s)

main2 = do
    din <- readFile "02.txt"
    let cmds = map (\x -> (x !! 0, read (x!!1) :: Int)) $ map words (lines din)
    let (a,b,_) = foldl (\pos cmd -> sim2 cmd pos) (0,0,0) cmds
    print(a*b)
