import System.IO

import qualified Data.List as List

fn' x = if x=='.' then 0 else 1

process (grid,def) fline = (grid3, fline!!def1)
    where
    combine3 (v1:v2:v3:vv) = reverse$ foldl (\curr@(c:_) x -> ((2*(mod c 4))+x):curr) ([(v1*4)+(v2*2)+v3]) vv
    
    def1 = def*4 + def*2 + def
    row0 = map (\_ -> def1) [1..(length (grid!!0))+2]

    grid1@(r1:r2:r3:rr) = row0:row0:(map (\x -> combine3 (def:def:x++[def,def])) grid)++[row0,row0] 
    grid2 = reverse$ foldl (\(curr@(c:_)) x -> ((tail c)++[x]):curr) [[r1,r2,r3]] rr
    grid3 = map (\ll -> map (\[a,b,c]->fline!!((a*64)+(b*8)+c)) (List.transpose ll)) grid2

main1 grid fline times = let (a,_)=foldl (\curr _-> process curr fline) (grid,0) [1..times] in sum (map sum a)

main = do
    din <- readFile "20.txt"
    let ll = lines din
    let fline = map fn' (ll!!0)
    let (_:_:grid0) = ll

    let grid = map (\l -> map fn' l) grid0
    print(main1 grid fline 50)
