import System.IO

import qualified Data.Set as Set

repl '=' = ' '
repl ',' = ' '
repl c = c

to_pts ss = (xx!!0, xx!!1)
    where xx = map (\n -> read n::Int) (words$ (map repl ss))
to_cmd ss = (xx!!0, read (xx!!1)::Int)
    where xx = words$ map repl$ (words ss)!!2

do_cmd pts (cmd_dir, val)
    | cmd_dir == "y" = Set.union (Set.filter (\(_,y)->y<val) pts) (Set.map (\(x,y)->(x,val-(y-val))) (Set.filter (\(_,y)->y>val) pts))
    | cmd_dir == "x" = Set.union (Set.filter (\(x,_)->x<val) pts) (Set.map (\(x,y)->(val-(x-val),y)) (Set.filter (\(x,_)->x>val) pts))

do_print pts = map (\y->map (\x->if Set.member (x,y) pts then '#' else '.') [0..38]) [0..5]

main = do
    din <- readFile "13.txt"
    let (r_pts, r_cmds) = span (/="") (lines din)
    let pts = Set.fromList$ map to_pts r_pts
    let cmds = map to_cmd (filter (/="") r_cmds)
    print(length$ do_cmd pts (cmds!!0))

    let final = foldl (\acc cmd -> do_cmd acc cmd) pts cmds
    putStr$ unlines$ do_print final
    print(0)
