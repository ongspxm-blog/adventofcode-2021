import System.IO
import Data.Char
import Data.List

import qualified Data.Map as Map

process0 grid = grid1
    where
    fn vals = foldl fn' [] (foldl fn' [] vals)
        where
        fn' [] x = [x]
        fn' acc@(l:ls) x = (if l>9 then x+1 else x):acc
    grid0 = map fn (map (map (+1)) grid)
    grid1 = transpose$ map fn (transpose grid0)

unwrap :: [[Int]] -> (Map.Map (Int, Int) Int, Int, Int)
unwrap grid = ((Map.fromList$ concat$ map (\(y,xs) -> zip [(x,y) | x<-[0..]] xs)$ zip [0..] grid), (length$ grid!!0), (length grid))

countshine grid = length$ Map.filter (==0) grid

process grid ww hh = process' (Map.map (+1) grid)
    where
    valid (a,b) = filter (\(x,y) -> x>=0 && x<ww && y>=0 && y<hh)$
        [(x+a,y+b) | x<-[-1..1],y<-[-1..1],(x/=0 || y/=0)]
    flash (a,b) grid0 = foldl (\grid1 pos -> Map.insertWith (+) pos 1 grid1)
        (Map.insert (a,b) 0 grid0)$
        filter (\pos -> let val = grid0 Map.! pos in val<10 && val>0) (valid (a,b))

    process' grid = if edited then process' grid2 else grid2
        where
        (edited, grid1) = foldl (\(bb,grid0) pos -> if grid0 Map.! pos == 10 then
            (True,flash pos grid0) else (bb,grid0)) (False, grid)
            [(x,y) | x<-[0..(ww-1)], y<-[0..(hh-1)]]
        grid2 = Map.map (\x->if x==11 then 0 else x) grid1

regrid grid ww hh =  map (\y -> map (\x ->grid Map.! (x,y)) [0..(ww-1)]) [0..(hh-1)]

fn2 din = fn' grid 1 
    where
    (grid, ww, hh) = unwrap$ map (\r -> map (\v -> ord v-ord '0') r) (lines din)
    fn' grid0 i = if (countshine grid1 == 100) then i else fn' grid1 (i+1) where grid1 = process grid0 ww hh

main = do
    din <- readFile "11.txt"
    print(fn2 din)
