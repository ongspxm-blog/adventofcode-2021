import System.IO
import Debug.Trace

import qualified Data.Map as Map

{-
type State = ([Int], [Char])
start = Map.fromList (zip "wxyzt" (repeat 0))

-- execute :: String -> State -> State
execute cmdstr state = execute' cmd a0 a2 state0
    where
    (cmd:a0:a1:_) = words (cmdstr++" 0")
    tt = a1=="w" || a1=="x" || a1=="y" || a1=="z"
    a2 = if tt then a1 else "t"
    (s0,s1) = state
    state0 = if tt then state else ((Map.insert 't' (read a1::Int) s0), s1)
execute' "inp" _ _ (state, (c:inputs1)) = (Map.insert 'w' (read [c]::Int) state, inputs1)
execute' "add" (a:_) (b:_) (state, inputs) = ((Map.insert a (
    (state Map.! a) + (state Map.! b)) state), inputs)
execute' "mul" (a:_) (b:_) (state, inputs) = ((Map.insert a (
    (state Map.! a) * (state Map.! b)) state), inputs)
execute' "div" (a:_) (b:_) (state, inputs) = ((Map.insert a (
    div (state Map.! a) (state Map.! b)) state), inputs)
execute' "mod" (a:_) (b:_) (state, inputs) = ((Map.insert a (
    mod (state Map.! a) (state Map.! b)) state), inputs)
execute' "eql" (a:_) (b:_) (state, inputs) = ((Map.insert a (
    if (state Map.! a)==(state Map.! b) then 1 else 0) state), inputs)

main1 cmds testval
    | s1 /= "" = undefined
    | f == 0 = testval
    | ((mod testval 1000) == 0) && trace (show testval) False = undefined
    | otherwise = main1 cmds (testval-1)
    where
    (s0,s1) = foldl (\acc cmd0 -> execute cmd0 acc) (start,(show testval)) cmds
    f = s0 Map.! 'z'
-}

process_cmd [] curr = zip [0..] curr
process_cmd cmds curr = process_cmd b (curr++[(d,e)])
    where
    a = take 18 cmds
    b = drop 18 cmds

    fn i = read (last$ words (a!!i)) :: Int
    d = fn 5
    e = fn 15

execute_1 [] [] vals = foldl (\acc x -> acc*10+x) 0 (map (vals Map.!) [0..13])
execute_1 [] (curr:cmds0) vals = execute_1 [curr] cmds0 vals
execute_1 state@(prev:state0) (curr:cmds0) vals
    | x1 > 9 =  execute_1 (curr:state) cmds0 vals
    | vv > 0 = execute_1 state0 cmds0 (Map.insert i1 9 (Map.insert i0 (9-vv) vals))
    | otherwise = execute_1 state0 cmds0 (Map.insert i0 9 (Map.insert i1 (9+vv) vals))
    where
    (i0, (x0,y0)) = prev
    (i1, (x1,y1)) = curr
    vv = y0+x1

execute_2 [] [] vals = foldl (\acc x -> acc*10+x) 0 (map (vals Map.!) [0..13])
execute_2 [] (curr:cmds0) vals = execute_2 [curr] cmds0 vals
execute_2 state@(prev:state0) (curr:cmds0) vals
    | x1 > 9 =  execute_2 (curr:state) cmds0 vals
    | vv > 0 = execute_2 state0 cmds0 (Map.insert i1 (1+vv) (Map.insert i0 1 vals))
    | otherwise = execute_2 state0 cmds0 (Map.insert i0 (1-vv) (Map.insert i1 1 vals))
    where
    (i0, (x0,y0)) = prev
    (i1, (x1,y1)) = curr
    vv = y0+x1


main = do
    din <- readFile "24.txt"
    let cmds = process_cmd (lines din) []
    print(execute_2 [] cmds Map.empty)
