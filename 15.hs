import System.IO

import qualified Data.Maybe as Maybe
import qualified Data.Set as Set
import qualified Data.Map as Map
import qualified Data.Heap as Heap
import qualified Data.Char as Char

getvalid grid (x,y) = filter (\p -> Map.member p grid) [(x-1,y),(x+1,y),(x,y-1),(x,y+1)]

dijkstra :: Map.Map (Int,Int) Int -> (Int,Int) -> Heap.MinHeap (Int,(Int,Int)) -> Set.Set (Int,Int) -> Int
dijkstra graph final heap visited
    | curr == final = v
    | Set.member curr visited = dijkstra graph final heap2 visited
    | otherwise = dijkstra graph final heap3 (Set.insert curr visited)
    where
    (v,curr) = Maybe.fromJust (Heap.viewHead heap)
    heap2 = Heap.drop 1 heap
    heap3 = foldl (\acc x -> if (Set.member x visited) then acc else (Heap.insert (v+(graph Map.! x), x) acc)) heap2 (getvalid graph curr)

main1 r_grid = dijkstra grid goal (Heap.singleton (0,(0,0)) :: Heap.MinHeap (Int,(Int,Int))) Set.empty
    where
    grid = Map.fromList$ concat$ map (\(y,xx)->map (\(x,v) -> ((x,y),v)) (zip [0..] xx)) (zip [0..] (map (\ll -> map (\x-> Char.ord x - Char.ord '0') ll) r_grid))
    goal = (length (r_grid!!0)-1, length r_grid-1)

-- the weird non-mod fn is a killer
main2 r_grid = dijkstra grid goal (Heap.singleton (0,(0,0)) :: Heap.MinHeap (Int,(Int,Int))) Set.empty
    where
    (n,h,w) = (4,length (r_grid!!0), length r_grid)
    grid = Map.fromList$ concat$ map (\(y,xx)-> concat$ map (\(x,v) ->
        [((x+(dx*w),y+(dy*h)), let vv=v+dy+dx in mod vv 10 + (if vv>9 then 1 else 0)) | dx<-[0..n],dy<-[0..n]]) (zip [0..] xx)) (zip [0..] (map (\ll -> map (\x-> Char.ord x - Char.ord '0') ll) r_grid))
    goal = (h*(n+1)-1, w*(n+1)-1)

main = do
    din <- readFile "15.txt"
    print(main2$ lines din)

