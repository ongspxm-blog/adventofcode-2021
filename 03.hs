import System.IO
import Data.Char

import Debug.Trace

bin2dec lst = sum $ map (\x -> (2^x) * (lst1!!x)) [0..11]
    where lst1 = reverse lst

soln1 vals = gm*ep
    where
        sum = foldl (\c x -> [x!!i + c!!i | i<-[0..(length x-1)]]) [0| i<-[1..12]] vals
        gm = bin2dec $ map (\x -> if x>500 then 1 else 0) sum
        ep = bin2dec $ map (\x -> if x<500 then 1 else 0) sum

findy a b vals pos
    | trace (show a ++ " " ++ show b ++ " " ++ show pos ++ " " ++ show (length vals)
        ++ " " ++ show cbit ++ "." ++ show cnt1 ++ "\n" ++ show vals ++ "\n" ++ show ans) False = undefined
    |otherwise = if (length ans) == 1 then ans!!0 else findy a b ans (pos+1)
    where
        cnt1 = sum $ map (!!pos) vals
        cnt0 = length vals - cnt1 
        cbit = if cnt0>cnt1 then a else b
        ans = filter (\x -> x!!pos == cbit) vals

find_most vals pos = findy 0 1 vals pos
find_less vals pos = findy 1 0 vals pos
soln2 vals = (bin2dec$ find_most vals 0) * (bin2dec$ find_less vals 0)

main=do
    din <- readFile "03.txt"
    let vals = map (\w -> map (\c -> ord c - ord '0') w) (lines din)
    print(soln2 vals)
