import System.IO
import Debug.Trace

import qualified Data.Set as Set
import qualified Data.Map as Map
import qualified Data.Char as Char

repr '-' = ' '
repr c = c

addedge a b edges = Map.insertWith (++) b [a] (Map.insertWith (++) a [b] edges)

main1 din = fn "start" (Set.singleton "start")
    where
    dlines = map (\l -> words$ map repr l) (lines din)
    edges = Map.fromList$ map (\(a,bb)->(a,Set.fromList bb))$ Map.toList$ foldl (\mm [a,b] -> addedge a b mm) Map.empty dlines

    doinsert new curr = if Char.ord(new!!0)>97 then (Set.insert new curr) else curr
    fn curr visited
        | curr == "end" = 1
        | otherwise = ans
        where
        ans = Set.foldl (\acc next -> acc + fn next (doinsert next visited)) 0 potential
        potential = (Set.filter (\x -> not$ Set.member x visited) (edges Map.! curr))

main2 din = length$ Set.fromList$ fn "start" Set.empty False []
    where
    dlines = map (\l -> words$ map repr l) (lines din)
    edges = Map.fromList$ map (\(a,bb)->(a,Set.fromList bb))$ Map.toList$ foldl (\mm [a,b] -> addedge a b mm) Map.empty dlines

    islower x = Char.ord(x!!0)>97
    doinsert new curr = if (islower new) then (Set.insert new curr) else curr

    fn curr visited double path
        | curr == "end" = [unwords path1]
        -- | trace (show curr ++ " " ++ show visited ++ " " ++ show double ++ " " ++ show potential2) False = undefined
        | otherwise = ans1 ++ ans2
        where
        path1 = path ++ [curr]
        visited1 = doinsert curr visited
        potential1 = (Set.filter (\x -> not$ Set.member x visited1) (edges Map.! curr))
        ans1 = Set.foldl (\acc next -> acc ++ (fn next visited1 double path1)) [] potential1
        potential2 = if (islower curr && not double && curr/="start") then (Set.filter (\x -> not$ Set.member x visited) (edges Map.! curr)) else Set.empty
        ans2 = Set.foldl (\acc next -> acc ++ (fn next visited True path1)) [] potential2

main = do
    din <- readFile "12.txt"
    print(main2 din)
