import System.IO
import Debug.Trace

import qualified Data.Map as Map
import qualified Data.List as List

do_apply start rules = foldl fn (' ',"") start
    where
    fn (ca, curr) cb = if ca == ' ' then (cb, [cb]) else (
        cb, curr++(rules Map.! [ca,cb])++[cb])

do_apply2 start counts = foldl fn (' ',chars) start
    where
    chars = Map.map (\_->0)$ getcount$ foldl (\acc (k,_) -> k++acc) "" (Map.toList counts)
    fn (ca, curr) cb = (cb, if ca == ' ' then curr else Map.insertWith (+) cb 1 (Map.unionWith (+) curr (counts Map.! [ca,cb])))

getcount final = Map.fromList$ map (\x -> (x!!0, length x))$ List.group$ List.sort final
finalcalc cnts0 = cnt1-cnt0
    where
    cnts = List.sort$ map (\(a,b)->(b,a))$ Map.toList$ cnts0
    (cnt0, _) = head cnts
    (cnt1, _) = last cnts

main1 start rules = finalcalc (getcount final)
    where
    final = foldl (\acc _ -> let (_,xx)=do_apply acc rules in xx) start [1..10]

main2' start rules = finalcalc final
    where
    double rules0 = Map.fromList$ map (\(k@[c0,c1],s)-> (k,let (_,x)=do_apply (c0:s++[c1]) rules0 in (tail$ init$ x))) (Map.toList rules0)


    rules2 = double rules
    rules4 = double rules2
    rules8 = double rules4
    count8 = Map.fromList$ map (\(k,s)->(k,getcount s)) (Map.toList rules8)

    (_, after2) = do_apply start rules2
    (_, final) = do_apply2 after2 count8

main2 start rules = cnt2-cnt1
    where
    pairs = Map.map (\x->0) rules
    chars = foldr (\mm acc -> Map.union mm acc) Map.empty$ map (\([c1,c2],_) -> Map.fromList [(c1,0),(c2,0)]) (Map.toList rules)

    start0 = let (_,xx)=(foldl (\(c0,mm) c1 -> if c0==' ' then (c1,mm) else (c1,Map.insertWith (+) [c0,c1] 1 mm)) (' ',pairs) start) in xx
    rules0 = Map.fromList$ map (\(k@[k1,k2],r) -> (k,[[k1,r!!0],[r!!0,k2]])) (Map.toList rules)

    mapin :: (Ord a, Num b) => a -> b -> Map.Map a b -> Map.Map a b
    mapin = (Map.insertWith (+))

    update start1 = foldr (\(k,c) acc -> let [p1,p2]=(rules0 Map.! k) in (mapin p2 c (mapin p1 c acc))) pairs (Map.toList start1)

    final = foldr (\_ acc -> update acc) start0 [1..40]
    cnts0 = foldr (\([c0,c1],c) acc -> mapin c1 c$ mapin c0 c acc) chars (Map.toList final)
    cnts1 = foldr (\c acc -> mapin c (-1) acc) cnts0 [head start, last start]
    cnts2 = foldr (\c acc -> mapin c 1 acc) (Map.map (\x -> div x 2) cnts1) [head start, last start]

    cnts3 = List.sort$ map (\(a,b)->(b,a)) (Map.toList cnts2)
    (cnt1,_) = head cnts3
    (cnt2,_) = last cnts3

main = do
    din <- readFile "14.test"
    let (r_start, r_rules) = span (/="")$ lines din
    let start = r_start!!0
    let rules = Map.fromList$ map (\[a,_,b]->(a,b)) (map words (filter (/="") r_rules))
    print(main2' start rules)
