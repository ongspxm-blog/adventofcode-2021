import System.IO

import qualified Data.Map as Map

type Data = (Integer, Integer, [Char])
data Packet = PacketL Integer Integer Integer
    | PacketO Integer Integer [Packet]
    deriving (Show, Eq)

getVal :: Data -> Integer -> (Integer, Data)
getVal data0@(v0,c0,s0) numbits
    | c0 < numbits = let
        s:ss = s0; vals=Map.fromList (zip "0123456789ABCDEF" [0..]) in (
        getVal ((v0*(2^4))+(vals Map.! s), c0+4, ss) numbits)
    | otherwise = let left=c0-numbits;bbits=2^left in (
        div v0 bbits, (mod v0 bbits, left, s0))

parseL :: Integer -> Data -> (Integer, Data)
parseL curr data0 = let
    (v,data1)=getVal data0 5;
    c2=(curr*(2^4))+(mod v (2^4)) in (
        if v<(2^4) then (c2,data1)
        else (parseL c2 data1))

parseO :: Data -> ([Packet], Data)
parseO data0 = let (lid,data1) = getVal data0 1 in
    if lid == 0 then parseO0 data1 else parseO1 data1

parseO0 :: Data -> ([Packet], Data)
parseO0 data0 = let
    (ll,data1) = getVal data0 15;
    (data2,data3) = getVal data1 ll
    in (parseO0' (data2,ll,"") [], data3)
parseO0' data0 curr = let (p,data1@(_,c,_))=parse data0; ps=curr++[p] in
    if c == 0 then ps else parseO0' data1 ps

parseO1 :: Data -> ([Packet], Data)
parseO1 data0 = let (ll,data1) = getVal data0 11 in parseO1' data1 ll []
parseO1' data0 curr ps = let (p,data1)=parse data0; ps2=ps++[p] in
    if curr == 1 then (ps2,data1) else parseO1' data1 (curr-1) ps2

parse :: Data -> (Packet, Data)
parse data0
    | tid==4 = let (v,data3)=parseL 0 data2 in (PacketL ver tid v, data3)
    | otherwise = let (v,data3)=parseO data2 in (PacketO ver tid v, data3)
    where
    (ver, data1) = getVal data0 3
    (tid, data2) = getVal data1 3

getV1 :: Packet -> Integer
getV1 (PacketL v _ _) = v
getV1 (PacketO v _ ps) = v + sum (map getV1 ps)

getV2 :: Packet -> Integer
getV2 (PacketL _ 4 v) = v
getV2 (PacketO _ 0 ps) = sum$ map getV2 ps
getV2 (PacketO _ 1 ps) = foldl (\acc p -> acc*(getV2 p)) 1 ps
getV2 (PacketO _ 2 ps) = minimum (map getV2 ps)
getV2 (PacketO _ 3 ps) = maximum (map getV2 ps)
getV2 (PacketO _ 5 [p0,p1]) = let v0=getV2 p0; v1=getV2 p1 in if v0>v1 then 1 else 0
getV2 (PacketO _ 6 [p0,p1]) = let v0=getV2 p0; v1=getV2 p1 in if v0<v1 then 1 else 0
getV2 (PacketO _ 7 [p0,p1]) = let v0=getV2 p0; v1=getV2 p1 in if v0==v1 then 1 else 0

main2 str = let (p,_)=parse (0,0,str) in getV2 p

test0=(0,0,"D2FE28")
test1=(0,0,"38006F45291200")
test2=(0,0,"EE00D40C823060")

test20=(0,0,"C200B40A82")

main = do
    din <- readFile "16.txt"
    print(let (p,_)=parse (0,0,din) in getV2 p)
