import System.IO

readInt :: String -> Int
readInt = read

repl ',' = ' '
repl c = c

loop (a:b:c:d:e:f:g:h:i:[]) = [b,c,d,e,f,g,h+a,i,a]

main1 = do
    txt0 <- readFile "06.txt"
    let txt = map repl txt0
    let trig = map readInt (words txt)
    let vals = map (\x -> length (filter (==x) trig)) [0..8]
    let ans = foldl (\acc x -> loop acc) vals [1..256]
    print(sum ans)
