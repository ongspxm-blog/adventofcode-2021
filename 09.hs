import System.IO
import Data.Char
import Data.List

import Debug.Trace

repl c = ord c - ord '0'

lowpoints grid = sum ans
    where
    fn_r vals = map (\x -> (tot!!x)<(tot!!(x-1))&&(tot!!x)<(tot!!(x+1))) [1..length vals]
        where tot = 10:vals++[10]
    res_r = map fn_r grid
    res_c = transpose$ map fn_r (transpose grid)
    res = map (\(a,b)->map (\(c,d) -> c&&d) (zip a b)) (zip res_r res_c)
    ans = map (\(a,b) -> sum$ map (\(c,d) -> if c then d+1 else 0)$
        zip a b)$ zip res grid

fn_row vals = fn$ fn vals
    where
    fn vals = foldl fn' [] vals
    fn' [] v = [v]
    fn' all@(l:ls) v = (if l>(-1) && l<v then l else v):all

cleanup grid = map (\(r,y) -> cleanup' r (y*yy)) (zip grid [0..])
    where
    yy = length (grid!!0)
    cleanup' vals x = map (\(a,b) -> if a==9 then -1 else b) (zip vals [x..(x + length vals)])

getareas grid = if (getsum grid)==(getsum grid2) then grid else getareas grid2 
    where
    getsum v = sum$ concat v
    dir0 v = map fn_row v
    dir1 v =  transpose$ map fn_row (transpose v)
    grid2 = dir1$ dir0 grid

gettop3 grid = foldl (\acc x -> acc*x) 1$ take 3$ reverse$ sort$ map
    (\r -> (if ((r!!0)/=(-1)) then (length r) else 0))$
    group$ sort$ concat$ getareas$ cleanup grid

main = do
    din <- readFile "09.txt"
    let dlines = map (\l -> map repl l) (lines din)
    print(gettop3 dlines)
